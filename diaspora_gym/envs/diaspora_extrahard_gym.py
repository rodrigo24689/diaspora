
# -*- coding: utf-8 -*-
import gym
from gym import error, spaces, utils
from gym.utils import seeding
import pandas as pd
import json
import re
class DiasporaEnvExHard(gym.Env):
  metadata = {'render.modes': ['human']}

  def __init__(self):
    self.actions_grid=['query_return','query_query_step','record_return','record_step','stop']
    self.actions_kb=["add_to_db_field","delete_db_field"]#ésto es una distribución no bernoulli
    self.kd_gold_standard=0
    self.web_data_frame = pd.DataFrame.from_csv("diaspora_gym/data/big.csv").drop_duplicates()
    self.persons=list(set(self.web_data_frame.id_person.tolist()))
    self.num_persons=len(self.persons)-1
    self.person_ind=-1
    self.querys_step=[]
    self.episodeDF=[]
    self.queryDF=[]
    self.queryDF_indexes=[]
    self.row_index_query=[]
    self.query_seen=[]
    self.agent_db=[]
    self.gold_standard=[]
    self.episode_continues=False
    self.num_steps_per_episode=0
    self.gold_json=json.load(open('diaspora_gym/data/gold_std.json','r'))['_default']
    self.words_organizations=open('diaspora_gym/data/jrc-organizations.txt','r').readlines()


    #Space variables



  def step(self, action,agent_db=None):
    """ ejecutamos el paso y regresamos el reward """
    """ debemos regresar un arreglo del tipo:
    respuesta:["estado","reward","booleando(si he termiando o no)"]"""
    action_grid,action_kb=action
    self.num_steps_per_episode+=1
    agent_db=self._action_db_selector(action_kb)
    query=self._action_grid_selector(action_grid)
    reward=self._get_reward()
    self.query_seen[self.query_indexes[self.query_ind]]+=1
    query_seen=self.query_seen[self.query_indexes[self.query_ind]]
    return [(query,self.num_steps_per_episode,query_seen,agent_db),reward,self.episode_continues]

  def reset(self):
    """ aquí avanzamos al siguiente episodio"""
    self.num_steps_per_episode=0
    if self.person_ind==self.num_persons:
      self.person_ind=0
    else:
      self.person_ind+=1
    self.agent_db=[[],[]]
    

    #set stepepisodeDFDF

    self.episodeDF = self.web_data_frame.loc[self.web_data_frame['id_person'] == self.persons[self.person_ind]].sort_values(['engine_search','number_snippet'])
    
    #set Query lists and indexes for queryDFs
    self.querys_step=list(set(self.episodeDF.search.tolist())) #Los querys
    self.query_indexes=list(range(len(self.querys_step))) #Los índices de la lista de querys
    self.row_index_query=[0]*len(self.querys_step) #En qué registro está cada query
    self.query_seen=[0]*len(self.querys_step) #Cuantos pasos he dado en ese query
    self.query_ind=0 #En qué query estoy

    self.querydf=self.episodeDF.loc[self.episodeDF["search"] == self.querys_step[self.query_indexes[self.query_ind]]]
    self.episode_continues=False

    #obtenemos el gold standard
    self.gold_std=self._get_gold_std()
    return self.render()

    
  def render(self, mode='human'):

    return self.querydf.iloc[self.row_index_query[self.query_indexes[self.query_ind]]].tolist(),self.agent_db

  def _action_grid_selector(self,action_grid):
      print(action_grid)
      if action_grid==0:
      #query_return is 0
        self._query_return()
      if action_grid==1:
      #query_step is 1
        self._query_query_step()
      if action_grid==2:
      #record_return is 2
        self._record_return()
      if action_grid==3:
      #record_return is 3  
        self._record_step()
      if action_grid==4:
      #stop is 4
        return self.querydf.iloc[self.row_index_query[self.query_indexes[self.query_ind]]].tolist()
      return self.querydf.iloc[self.row_index_query[self.query_indexes[self.query_ind]]].tolist()

  def _action_db_selector(self,action_selector):
    print(action_selector)
    if action_selector==0:
    #action 0 is push to db
      self._push_to_db()
    if action_selector==1:
    #action 1 is pop from db
      self._pop_from_db()
    if action_selector==2:
    #action 2 is do nothing on db
      return self.agent_db
    return self.agent_db

  def _push_to_db(self):
    text=" ".join(self.querydf.iloc[self.row_index_query[self.query_indexes[self.query_ind]]].tolist()[-2:])
    entities=self._get_entities(text)
    if len(entities[0])>0:
      self.agent_db[0].append(entities[0])
    if len(entities[1])>0:
      self.agent_db[1].append(entities[1])
    
  def _pop_from_db(self):
    try:
      self.agent_db[0].pop()
    except:
      pass
    try:
      self.agent_db[1].pop()
    except:
      pass
  def _get_entities(self,text):

    institutions=[]
    for word in self.words_organizations:
        idx=text.lower().find(word)
        if idx >= 0:
            word_finded=text[idx:idx+len(word)]
            institutions.append(word_finded.strip())
    years=re.findall("[12][901][0-9]{2}",text)
    if len(institutions)>0:
      institutions=institutions[0]
    if len(years)>0:
      years=years[0]
    return (institutions,years)
  def _get_gold_std(self):
    institution=self.gold_json[str(\
       self.persons[self.person_ind])]['institution'].lower()
    year=self.gold_json[str(\
       self.persons[self.person_ind])][u'year_finish']
    return [institution,year]
  def _loadInstituteRE(self):
    with open("../data/JRC-Organizations.normal.txt",'r') as fd:
      self.words_organizations=[]
      for line in fd.readlines():
        self.words_organizations.append(" "+line.strip().lower()+" ")
        self.words_organizations.append(" "+line.strip().lower())
        self.words_organizations.append(line.strip().lower()+" ")
  
  def _query_query_step(self):
    if self.query_ind==len(self.querys_step)-1:
      self.query_ind=self.query_ind
    else:
      self.query_ind+=1
  def _query_return(self):
    if self.query_ind==0:
      self.query_ind=self.query_indexes[-1]
    else:
      self.query_ind-=1
  def _record_return(self):
    
    if self.row_index_query[self.query_indexes[self.query_ind]]==0:
      self.row_index_query[self.query_indexes[self.query_ind]]=len(self.querydf.index)-1
    else:
      self.row_index_query[self.query_indexes[self.query_ind]]-=1

  def _record_step(self):
    
    if self.row_index_query[self.query_indexes[self.query_ind]]==len(self.querydf.index)-1:
      if self.query_ind==len(self.querys_step)-1:
        self.episode_continues=True
      else:
        self.row_index_query[self.query_indexes[self.query_ind]]=0
    else:
      self.row_index_query[self.query_indexes[self.query_ind]]+=1
    
  def _get_reward(self):
    """ calcular con agent_db & gold_std """
    if self.gold_std[0] in self.agent_db[0]:
      reward_institutes=1/len(self.agent_db[0])
    else:
      reward_institutes=0
    
    if self.gold_std[1] in self.agent_db[1]:
      reward_years=1/len(self.agent_db[1])
    else:
      reward_years=0
    return reward_institutes+reward_years-self.num_steps_per_episode
        