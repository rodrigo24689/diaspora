from gym.envs.registration import register

register(
    id='diaspora-v0',
    entry_point='diaspora_gym.envs:DiasporaEnv',
)
register(
    id='diaspora-extrahard-v0',
    entry_point='diaspora_gym.envs:DiasporaExtraHardEnv',
)

