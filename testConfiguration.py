import itertools
import math
from dopamine.discrete_domains.gym_lib import _basic_discrete_domain_network


import gym
import numpy as np
import tensorflow as tf

import gin.tf
import gin


gin.constant('diaspora.OBSERVATION_DTYPE', tf.float64)

@gin.configurable
def diaspora_dqn_network(num_actions, network_type, state):
  """Builds the deep network used to compute the agent's Q-values.
  It rescales the input features to a range that yields improved performance.
  Args:
    num_actions: int, number of actions.
    network_type: namedtuple, collection of expected values to return.
    state: `tf.Tensor`, contains the agent's current state.
  Returns:
    net: _network_type object containing the tensors output by the network.
  """
  q_values = _basic_discrete_domain_network(
       np.array([0., 0.]), np.array([99., 99.]), num_actions, state)
  return network_type(q_values)